<?php

namespace Webpnk\DndCharacter\Builder;

use Webpnk\DndCharacter\Entity\DndCharacter;
use Webpnk\DndCharacter\Entity\DndCharacterInterface;

class DndCharacterBuilder implements DndCharacterBuilderInterface
{
    protected int $strength = 0;
    protected int $dexterity = 0;
    protected int $constitution = 0;
    protected int $intelligence = 0;
    protected int $wisdom = 0;
    protected int $charisma = 0;

    public function set(string $property, int $value): static
    {
        $this->{$property} = $value;

        return $this;
    }

    public function get(): DndCharacterInterface
    {
        return new DndCharacter(
            $this->strength,
            $this->dexterity,
            $this->constitution,
            $this->intelligence,
            $this->wisdom,
            $this->charisma,
        );
    }
}