<?php

namespace Webpnk\DndCharacter\Builder;

use Webpnk\DndCharacter\Entity\DndCharacterInterface;

interface DndCharacterBuilderInterface
{
    public function set(string $property, int $value): static;

    public function get(): DndCharacterInterface;
}