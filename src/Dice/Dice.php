<?php

namespace Webpnk\DndCharacter\Dice;

use Webpnk\DndCharacter\Exceptions\DiceSidesCountShouldBeTwoOrMore;

class Dice implements DiceInterface
{
    /**
     * @throws DiceSidesCountShouldBeTwoOrMore
     */
    public function __construct(
        protected $sides = 6,
    ) {
        if ($this->sides < 2) {
            throw new DiceSidesCountShouldBeTwoOrMore;
        }
    }

    public function throw(): int
    {
        return rand(1, $this->sides);
    }
}