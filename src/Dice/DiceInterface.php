<?php

namespace Webpnk\DndCharacter\Dice;

interface DiceInterface
{
    public function throw(): int;
}