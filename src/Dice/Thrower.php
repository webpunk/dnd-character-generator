<?php

namespace Webpnk\DndCharacter\Dice;

use Webpnk\DndCharacter\Exceptions\CanNotThrowNotPositiveDicesAmount;

class Thrower implements ThrowerInterface
{
    private ThrownDiceCollection $collection;

    public function __construct(protected DiceInterface $dice)
    {
        $this->collection = new ThrownDiceCollection;
    }

    public function throw(int $amount): ThrownDiceCollectionInterface
    {
        if ($amount <= 0) {
            throw new CanNotThrowNotPositiveDicesAmount;
        }

        for ($i = 0; $i < $amount; $i++) {
            $this->collection->push($this->dice->throw());
        }

        return $this->collection;
    }
}