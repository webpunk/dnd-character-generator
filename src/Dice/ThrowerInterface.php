<?php

namespace Webpnk\DndCharacter\Dice;

use Webpnk\DndCharacter\Exceptions\CanNotThrowNotPositiveDicesAmount;
use Webpnk\DndCharacter\Support\CollectionInterface;

interface ThrowerInterface
{
    /**
     * @param int $amount
     * @return CollectionInterface<int>
     *
     * @throws CanNotThrowNotPositiveDicesAmount
     */
    public function throw(int $amount): ThrownDiceCollectionInterface;
}