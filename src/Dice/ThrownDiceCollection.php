<?php

namespace Webpnk\DndCharacter\Dice;

use Webpnk\DndCharacter\Support\BaseCollection;
use Webpnk\DndCharacter\Support\CollectionInterface;

/**
 * @implements CollectionInterface<int>
 */
class ThrownDiceCollection extends BaseCollection implements ThrownDiceCollectionInterface
{
    public function sort(): static
    {
        rsort($this->values);

        return $this;
    }

    public function sum(): int
    {
        return array_sum($this->values);
    }
}