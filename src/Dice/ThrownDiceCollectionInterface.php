<?php

namespace Webpnk\DndCharacter\Dice;

use Webpnk\DndCharacter\Support\BaseCollection;
use Webpnk\DndCharacter\Support\CollectionInterface;

interface ThrownDiceCollectionInterface extends CollectionInterface
{
    public function sum(): int;
}