<?php

namespace Webpnk\DndCharacter\Entity;

class DndCharacter implements DndCharacterInterface
{
    const DEFAULT_HIT_POINTS = 10;

    public function __construct(
        protected int $strength,
        protected int $dexterity,
        protected int $constitution,
        protected int $intelligence,
        protected int $wisdom,
        protected int $charisma,
    ) {}

    public function getHitPoints(): int
    {
        return self::DEFAULT_HIT_POINTS + $this->getConstitutionModifier();
    }

    protected function getConstitutionModifier(): int
    {
        return intval(
            floor(($this->constitution - 10) / 2)
        );
    }

    public function getStrength(): int
    {
        return $this->strength;
    }

    public function getDexterity(): int
    {
        return $this->dexterity;
    }

    public function getConstitution(): int
    {
        return $this->constitution;
    }

    public function getIntelligence(): int
    {
        return $this->intelligence;
    }

    public function getWisdom(): int
    {
        return $this->wisdom;
    }

    public function getCharisma(): int
    {
        return $this->charisma;
    }
}