<?php

namespace Webpnk\DndCharacter\Entity;

interface DndCharacterInterface
{
    public function getHitPoints(): int;

    public function getStrength(): int;

    public function getDexterity(): int;

    public function getConstitution(): int;

    public function getIntelligence(): int;

    public function getWisdom(): int;

    public function getCharisma(): int;
}