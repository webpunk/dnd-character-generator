<?php

namespace Webpnk\DndCharacter\Exceptions;

class DiceSidesCountShouldBeTwoOrMore extends \Exception
{
}