<?php

namespace Webpnk\DndCharacter\Generator;

use Webpnk\DndCharacter\Entity\DndCharacterInterface;

interface DndCharacterGeneratorInterface
{
    public function generate(): DndCharacterInterface;
}