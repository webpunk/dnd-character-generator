<?php

namespace Webpnk\DndCharacter\Generator;

use Webpnk\DndCharacter\Builder\DndCharacterBuilderInterface;
use Webpnk\DndCharacter\Entity\DndCharacterInterface;
use Webpnk\DndCharacter\Strategies\ParameterValueGeneratorInterface;

class DndCharacterRandomGenerator implements DndCharacterGeneratorInterface
{
    public function __construct(
        protected ParameterValueGeneratorInterface $valueGenerator,
        protected DndCharacterBuilderInterface $builder,
    ) {}

    public function generate(): DndCharacterInterface
    {
        return $this->builder
            ->set('strength', $this->valueGenerator->generate())
            ->set('dexterity', $this->valueGenerator->generate())
            ->set('constitution', $this->valueGenerator->generate())
            ->set('intelligence', $this->valueGenerator->generate())
            ->set('wisdom', $this->valueGenerator->generate())
            ->set('charisma', $this->valueGenerator->generate())
            ->get();
    }
}