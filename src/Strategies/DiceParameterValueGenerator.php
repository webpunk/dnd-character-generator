<?php

namespace Webpnk\DndCharacter\Strategies;

use Webpnk\DndCharacter\Dice\ThrowerInterface;
use Webpnk\DndCharacter\Exceptions\CanNotThrowNotPositiveDicesAmount;

class DiceParameterValueGenerator implements ParameterValueGeneratorInterface
{
    public function __construct(
        protected ThrowerInterface $thrower,
    ) {}

    /**
     * @throws CanNotThrowNotPositiveDicesAmount
     */
    public function generate(): int
    {
        return $this->thrower
            ->throw(4)
            ->sort()
            ->take(3)
            ->sum();
    }
}