<?php

namespace Webpnk\DndCharacter\Strategies;

interface ParameterValueGeneratorInterface
{
    public function generate(): int;
}