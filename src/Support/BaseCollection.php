<?php

namespace Webpnk\DndCharacter\Support;

/**
 * @template T
 */
abstract class BaseCollection implements CollectionInterface
{
    /**
     * @var T[] $values
     */
    public function __construct(
        protected iterable $values = [],
    ) {}

    /**
     * @param T $value
     * @return $this
     */
    public function push(mixed $value): static
    {
        $this->values []= $value;

        return $this;
    }

    public function take(int $amount): static
    {
        $this->values = array_slice($this->values, 0, $amount);

        return $this;
    }

    public function toArray(): array
    {
        return $this->values;
    }

    public function getIterator(): \Traversable
    {
        return new \ArrayIterator($this->values);
    }
}