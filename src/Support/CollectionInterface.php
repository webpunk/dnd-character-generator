<?php

namespace Webpnk\DndCharacter\Support;

/**
 * @template T
 */
interface CollectionInterface extends \IteratorAggregate
{
    /**
     * @param T $value
     * @return $this
     */
    public function push(mixed $value): static;

    public function sort(): static;

    public function take(int $amount): static;

    /**
     * @return T[]
     */
    public function toArray(): array;
}