<?php

namespace Tests;

use Webpnk\DndCharacter\Builder\DndCharacterBuilder;
use Webpnk\DndCharacter\Entity\DndCharacterInterface;

class CharacterBuilderTest extends TestCase
{
    public function test_character_builder_can_build_character()
    {
        $builder = new DndCharacterBuilder;

        $character = $builder
            ->set('strength', 10)
            ->set('wisdom', 10)
            ->get();

        $this->assertInstanceOf(DndCharacterInterface::class, $character);
        $this->assertEquals(10, $character->getStrength());
        $this->assertEquals(10, $character->getWisdom());
        $this->assertEquals(0, $character->getCharisma());
    }
}