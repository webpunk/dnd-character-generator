<?php

namespace Tests;

use Webpnk\DndCharacter\Builder\DndCharacterBuilder;
use Webpnk\DndCharacter\Dice\Dice;
use Webpnk\DndCharacter\Dice\Thrower;
use Webpnk\DndCharacter\Entity\DndCharacterInterface;
use Webpnk\DndCharacter\Exceptions\CanNotThrowNotPositiveDicesAmount;
use Webpnk\DndCharacter\Generator\DndCharacterRandomGenerator;
use Webpnk\DndCharacter\Strategies\DiceParameterValueGenerator;

class CharacterGeneratorTest extends TestCase
{
    public function test_generator_can_generate_character()
    {
        $thrower = new Thrower(new Dice);
        $builder = new DndCharacterBuilder;
        $parameterGenerator = new DiceParameterValueGenerator($thrower);
        $generator = new DndCharacterRandomGenerator($parameterGenerator, $builder);

        $character = $generator->generate();

        $this->assertInstanceOf(DndCharacterInterface::class, $character);
        $this->assertGreaterThan(0, $character->getCharisma());
        $this->assertGreaterThan(0, $character->getWisdom());
        $this->assertGreaterThan(0, $character->getStrength());
        $this->assertGreaterThan(0, $character->getHitPoints());
        $this->assertGreaterThan(0, $character->getConstitution());
        $this->assertGreaterThan(0, $character->getDexterity());
        $this->assertGreaterThan(0, $character->getIntelligence());
    }
}