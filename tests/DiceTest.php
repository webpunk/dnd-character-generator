<?php

namespace Tests;

use Webpnk\DndCharacter\Dice\Dice;
use Webpnk\DndCharacter\Exceptions\DiceSidesCountShouldBeTwoOrMore;

class DiceTest extends TestCase
{
    /**
     * @throws DiceSidesCountShouldBeTwoOrMore
     */
    public function test_it_can_throw_dice_and_get_result_from_1_to_dice_sides_count()
    {
        $dice = new Dice(6);

        $result = $dice->throw();

        $this->assertLessThanOrEqual(6, $result);
        $this->assertGreaterThan(0, $result);
    }

    public function test_it_can_not_create_dice_with_sides_count_less_than_two()
    {
        $this->expectException(DiceSidesCountShouldBeTwoOrMore::class);

        new Dice(1);

        new Dice(-1);

        new Dice(0);
    }
}