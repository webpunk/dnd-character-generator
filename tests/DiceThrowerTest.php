<?php

namespace Tests;

use Webpnk\DndCharacter\Dice\Dice;
use Webpnk\DndCharacter\Dice\Thrower;
use Webpnk\DndCharacter\Exceptions\CanNotThrowNotPositiveDicesAmount;

class DiceThrowerTest extends TestCase
{
    /**
     * @throws CanNotThrowNotPositiveDicesAmount
     */
    public function test_thrower_can_throw_dices()
    {
        $thrower = new Thrower(new Dice());

        $thrownCollection = $thrower->throw(5);

        $this->assertCount(5, $thrownCollection);
    }

    public function test_it_can_not_throw_0_dices()
    {
        $this->expectException(CanNotThrowNotPositiveDicesAmount::class);

        $thrower = new Thrower(new Dice());

        $thrower->throw(0);
    }

    public function test_it_can_not_throw_negative_amount_dices()
    {
        $this->expectException(CanNotThrowNotPositiveDicesAmount::class);

        $thrower = new Thrower(new Dice());

        $thrower->throw(-1);
    }
}