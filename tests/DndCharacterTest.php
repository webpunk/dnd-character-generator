<?php

namespace Tests;

use Webpnk\DndCharacter\Entity\DndCharacter;

class DndCharacterTest extends TestCase
{
    public function test_character_has_proper_constitution_modifier()
    {
        $character = new class (14, 11, 3, 14, 12, 18) extends DndCharacter {
            public function getConstitutionModifier(): int
            {
                return parent::getConstitutionModifier();
            }
        };

        $this->assertEquals(-4, $character->getConstitutionModifier());
    }

    public function test_character_has_proper_hit_points()
    {
        $character = new DndCharacter(14, 11, 3, 14, 12, 18);

        $this->assertEquals(6, $character->getHitPoints());
    }
}