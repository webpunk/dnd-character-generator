<?php

namespace Tests;

use Webpnk\DndCharacter\Entity\DndCharacter;

abstract class TestCase extends \PHPUnit\Framework\TestCase
{
    protected function makeCharacter(): DndCharacter
    {
        return new DndCharacter(
            rand(1, 6),
            rand(1, 6),
            rand(1, 6),
            rand(1, 6),
            rand(1, 6),
        );
    }
}