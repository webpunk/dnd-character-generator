<?php

namespace Tests;

use Webpnk\DndCharacter\Dice\ThrownDiceCollection;

class ThrownDiceCollectionTest extends TestCase
{
    public function test_created_collection_is_iterable()
    {
        $collection = new ThrownDiceCollection;

        $this->assertIsIterable($collection);
    }

    public function test_value_can_be_pushed_to_collection()
    {
        $collection = new ThrownDiceCollection;
        $this->assertCount(0, $collection);

        $collection->push(1);
        $this->assertCount(1, $collection);

        $collection->push(1);
        $this->assertCount(2, $collection);
    }

    public function test_collection_can_be_sorted_by_values_desc()
    {
        $collection = new ThrownDiceCollection([1, 3, 6, 2, 9, 4, 5, 7, 8]);
        $this->assertSame([1, 3, 6, 2, 9, 4, 5, 7, 8], $collection->toArray());

        $collection->sort();

        $this->assertSame([9, 8, 7, 6, 5, 4, 3, 2, 1], $collection->toArray());
    }

    public function test_it_can_take_sub_collection()
    {
        $collection = new ThrownDiceCollection([1, 3, 6, 2, 9, 4, 5, 7, 8]);
        $collection->take(3);

        $this->assertSame([1, 3, 6], $collection->toArray());
    }

    public function test_it_can_calculate_correct_sum_from_collection_values()
    {
        $collection = new ThrownDiceCollection([1, 2, 3]);

        $this->assertEquals(6, $collection->sum());
    }

    public function test_collection_could_be_converted_to_array()
    {
        $collection = new ThrownDiceCollection([1, 2, 3]);

        $this->assertSame([1, 2, 3], $collection->toArray());
    }
}